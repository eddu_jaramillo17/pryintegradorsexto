<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{
   //aqui colocamos las propiedades de las tablas que  si requeremos que sean requeridas
  
    

    public function index()
    {
        $almacenarProducto=Producto::all();
        return view('producto.index',compact('almacenarProducto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        return view('producto.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
         $datos=[
      'pro_descripcion'=>'required | string | max:255',
      'pro_precio'=>'required | string | max:255',
      'pro_stock'=>'required | string | max:255',
      'pro_medida'=>'required | string | max:255',  
      'pro_estado'=>'required | string | max:255'];

        //referenciamos a nuestra clase con $this lo que hacemos es llamar al metodo validate y le pasamo la request osea los datos del formulario recuperados y le pasamos nuestra propiedades de la tabla y los requerimientos que le pusimos
        $this->validate($request,$datos);
        //ahora llamamos a nuestro modelo
         Producto::create($request->all());//llamamos a nuestro modelo y al metodo crear y le eviamos que queremos que nos ingrese todo del formulario
        return redirect('producto');// y aqui hacemos de mandar a producto una ves que termine
        
       }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
