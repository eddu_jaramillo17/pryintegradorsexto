<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable=["pro_descripcion","pro_precio","pro_stock","pro_medida","pro_estado"];
}
