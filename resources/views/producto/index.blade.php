@extends("layouts.headeradmin")


@section("contenido")




 <!-- /.card-header -->
            <div class="card-body">
              <table id="producto" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Descripcion</th>
                  <th>Precio</th>
                  <th>Stock</th>
                  <th>Medida</th>
                  <th>Estado</th>
                </tr>
                </thead>
                <tbody>
                  @forelse($almacenarProducto as $recorrer)
                  <tr>
                    <td>{{ $recorrer->pro_descripcion }}</td>
                    <td>{{ $recorrer->pro_precio }}</td>
                    <td>{{ $recorrer->pro_stock }}</td>
                    <td>{{ $recorrer->pro_medida }}</td>
                    <td>{{ $recorrer->pro_estado }}</td>
            
                  </tr>
                 @empty
                 <div class="alert alert-info" role="alert">No hay resultados</div>
                 @endforelse
                </tbody>
              </table>
            </div>





@endsection


@section("footer")



@endsection
